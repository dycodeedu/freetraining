//
//  HomeViewController.swift
//  First
//
//  Created by danixsofyan on 16/07/18.
//  Copyright © 2018 DyCodeEdu. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDataSource {

    final let url = URL(string: "http://microblogging.wingnity.com/JSONParsingTutorial/jsonActors")
    
     var actors = [Actor]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        downloadJSON()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadJSON() {
        guard let downloadURL = url else { return }
        URLSession.shared.dataTask(with: downloadURL) { data, urlResponse, error in
            guard let data = data, error == nil, urlResponse != nil else {
                print("Something Worng")
                return
            }
            print("downloaded")
            do {
                let decoder = JSONDecoder()
                let downloadedActors = try decoder.decode(Actors.self, from: data)
                self.actors = downloadedActors.actors
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } catch {
                print("Something Worng after download")
            }
            }.resume()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ActorCell") as? ActorTableViewCell else { return UITableViewCell() }
        cell.nameLabel.text = "Name: " + actors[indexPath.row].name
        cell.DOBLabel.text = "DOB: " + actors[indexPath.row].dob
        
        if let imageURL = URL(string: actors[indexPath.row].image){
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: imageURL)
                if let data = data {
                    let image = UIImage(data: data)
                    DispatchQueue.main.async {
                        cell.imgView.image = image
                    }
                }
            }
        }
        return cell
    }

}
