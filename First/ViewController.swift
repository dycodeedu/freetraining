//
//  ViewController.swift
//  First
//
//  Created by danixsofyan on 16/07/18.
//  Copyright © 2018 DyCodeEdu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func loginButtonTaped(_ sender: UIButton) {
        login()
    }
    
    @IBAction func forgetButtonTapped(_ sender: UIButton) {
        let vcForget = storyboard?.instantiateViewController(withIdentifier: "forget") as! ForgetViewController
            vcForget.email = emailTextfield.text
        navigationController?.pushViewController(vcForget, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func login() {
        view.endEditing(true)
        guard
        let email = emailTextfield.text, email == "dani@dycode.com",
        let password = passwordTextfield.text, password == "a"
            else {
                let alert = UIAlertController(title: "Error", message: "Invalid email or password", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(alert, animated: true, completion: nil)
                return
        }
        let vc = storyboard?.instantiateViewController(withIdentifier: "home")
        navigationController?.pushViewController(vc!, animated: true)
    }

}

